#define DEBUG

using System.Diagnostics.CodeAnalysis;

namespace TicTacToe;

public static class Debug
{

    [ExcludeFromCodeCoverage]
    public static void Main(string[] args)
    {
        Game game = new Game();
        game.Start();
    }

}
