﻿namespace TicTacToe;

public class Game
{
    public char[] board {get; set;}
    public int currentPlayer {get; set;}
    public bool gameOver {get; set;}
    public int winner {get; set;}

    public Game()
    {
        board = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
        currentPlayer = 1;
        gameOver = false;
    }

    public void Start()
    {
        while (!gameOver)
        {
            DrawBoard();
            int choice = GetPlayerChoice();
            UpdateBoard(choice);
            CheckForWinner();
            SwitchPlayer();
        }
    }

    private void DrawBoard()
    {

        Console.Clear();

        Console.WriteLine("Tic Tac Toe");
        Console.WriteLine("Player 1 (X)  -  Player 2 (O)\n");

        Console.WriteLine($" {board[0]} | {board[1]} | {board[2]} ");
        Console.WriteLine("---|---|---");
        Console.WriteLine($" {board[3]} | {board[4]} | {board[5]} ");
        Console.WriteLine("---|---|---");
        Console.WriteLine($" {board[6]} | {board[7]} | {board[8]} ");
        Console.WriteLine();
    }

    private int GetPlayerChoice()
    {
        int choice;
        bool isValid = false;

        do
        {
            Console.WriteLine($"Player {currentPlayer}'s turn. Enter a number (1-9):");
            isValid = int.TryParse(Console.ReadLine(), out choice);
            if (!isValid || choice < 1 || choice > 9 || board[choice - 1] == 'X' || board[choice - 1] == 'O')
            {
                Console.WriteLine("Invalid input. Please try again.");
                isValid = false;
            }
        } while (!isValid);

        return choice;
    }

    public void UpdateBoard(int choice)
    {
        char symbol = (currentPlayer == 1) ? 'X' : 'O';
        board[choice - 1] = symbol;
    }

    public void CheckForWinner()
    {
        if ((board[0] == board[1] && board[1] == board[2]) ||
            (board[3] == board[4] && board[4] == board[5]) ||
            (board[6] == board[7] && board[7] == board[8]) ||
            (board[0] == board[3] && board[3] == board[6]) ||
            (board[1] == board[4] && board[4] == board[7]) ||
            (board[2] == board[5] && board[5] == board[8]) ||
            (board[0] == board[4] && board[4] == board[8]) ||
            (board[2] == board[4] && board[4] == board[6]))
        {
            DrawBoard();
            Console.WriteLine($"Player {currentPlayer} wins!");
            winner = currentPlayer;
            gameOver = true;
        }
        else if (!board.Contains('1') &&
                    !board.Contains('2') &&
                    !board.Contains('3') &&
                    !board.Contains('4') &&
                    !board.Contains('5') &&
                    !board.Contains('6') &&
                    !board.Contains('7') &&
                    !board.Contains('8') &&
                    !board.Contains('9'))
        {
            DrawBoard();
            Console.WriteLine("It's a draw!");
            winner = 0;
            gameOver = true;
        }
    }

    public void SwitchPlayer()
    {
        currentPlayer = (currentPlayer == 1) ? 2 : 1;
    }

}
