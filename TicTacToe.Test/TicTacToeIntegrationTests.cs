using TicTacToe;
using Xunit;

namespace TicTacToe.Test
{
    public class TicTacToeIntegrationTests
    {
        [Fact]
        public void TestGameFlow_Player1Wins()
        {
            // Arrange
            Game game = new Game();
            string input = "0\n1\n2\n4\n5\n7";
            Console.SetIn(new System.IO.StringReader(input));

            // Act
            game.Start();

            // Assert
            Assert.True(game.gameOver);
            Assert.Equal(1, game.winner);

        }

        [Fact]
        public void TestGameFlow_Draw()
        {
            // Arrange
            Game game = new Game();
            string input = "1\n2\n3\n4\n5\n7\n6\n9\n8";
            Console.SetIn(new System.IO.StringReader(input));

            // Act
            game.Start();

            // Assert
            Assert.True(game.gameOver);
            Assert.Equal(0, game.winner);
        }

    }
}
