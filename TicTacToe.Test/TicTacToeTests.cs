using TicTacToe;
using Xunit;

namespace TicTacToe.Test
{

    public class TicTacToeTests
    {
        [Fact]
        public void TestGameInitialization()
        {
            // Arrange
            Game game = new Game();

            // Act

            // Assert
            Assert.Equal('1', game.board[0]);
            Assert.Equal('2', game.board[1]);
            Assert.Equal('3', game.board[2]);
            Assert.Equal('4', game.board[3]);
            Assert.Equal('5', game.board[4]);
            Assert.Equal('6', game.board[5]);
            Assert.Equal('7', game.board[6]);
            Assert.Equal('8', game.board[7]);
            Assert.Equal('9', game.board[8]);
            Assert.Equal(1, game.currentPlayer);
            Assert.False(game.gameOver);
        }

        [Fact]
        public void TestUpdateBoard()
        {
            // Arrange
            Game game = new Game();
            int choice = 5;

            // Act
            game.UpdateBoard(choice);

            // Assert
            Assert.Equal('X', game.board[choice - 1]);
        }

        [Fact]
        public void TestSwitchPlayer()
        {
            // Arrange
            Game game = new Game();
            game.currentPlayer = 1;

            // Act
            game.SwitchPlayer();

            // Assert
            Assert.Equal(2, game.currentPlayer);

            // Act again
            game.SwitchPlayer();

            // Assert again
            Assert.Equal(1, game.currentPlayer);
        }

        [Theory]
        [InlineData(1, 2, 3)]
        [InlineData(4, 5, 6)]
        [InlineData(7, 8, 9)]
        [InlineData(1, 4, 7)]
        [InlineData(2, 5, 8)]
        [InlineData(3, 6, 9)]
        [InlineData(1, 5, 9)]
        [InlineData(3, 5, 7)]
        public void TestCheckForWinner(int cell1, int cell2, int cell3)
        {
            // Arrange
            Game game = new Game();
            game.board = new char[] { '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            game.board[cell1-1]='X';
            game.board[cell2-1]='X';
            game.board[cell3-1]='X';

            // Act
            game.CheckForWinner();

            // Assert
            Assert.True(game.gameOver);

        }

    }


}
